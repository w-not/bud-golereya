$(document).ready(function(){

	$('input[type=reset]').on('click', function (e) {
	    var $t = $(this),
	        target = $t[0].href || $t.data("target") || $t.parents('.modal') || [];

	  $(target)
	    .find(".inp-style,textarea,select")
	       .val('')
	       .end()
	    .find("input[type=checkbox], input[type=radio]")
	       .prop("checked", "")
	       .end();
	});
	
});