$(document).ready(function() {

	$(document).on("tap click", 'label a', function( event, data ){
	    event.stopPropagation();
	    event.preventDefault();
	    window.open($(this).attr('href'), $(this).attr('target'));
	    return false;
	});

	if($('.inp-decorate').length) {
		$('.inp-decorate').styler();
	}

	$('.jsSliderWinner').owlCarousel({
	    loop: false,
	    margin: 0,
	    nav: true,
		items: 1
	});
});

(function() {
	$(document).on('click', '.jsVideoPopup', function() {
		var source = $(this).data('src'),
		    html = '<iframe class="iframe-youtube" width="100%" height="100%" src="' +
			    source + '?autoplay=1&controls=2&showinfo=0" frameborder="0" allowfullscreen></iframe>';

		if (!source) {
			return;
		}

		$('#modal_video').modal('show');
		setTimeout(function() {
			$('#modal_video').find('.modal-iframe').html('<div class="video__iframe">' + html + '</div>');
		}, 500);

		return false;
	});

	$(document).on('hide.bs.modal', '#modal_video', function() {
		$(this).find('.modal-iframe').html('');
	});
})();