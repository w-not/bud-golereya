var ww = $(window).width(),
    wh = $(window).height(),
    scrollTo = (function() {
        const container = $('.jsScrollTo');

        if (!container.length) {
            return;
        }

        container.click(function() {
            let id = $(this).attr('href'),
                baseAnimate = $('html, body'),
                isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1,
                offset;

            if ($(id).offset() === undefined) {
                if (id === '#top') {
                    offset = $('.board').offset().top - 15;
                } else {
                    return false;
                }
            } else {
                offset = $(id).offset().top;
            }

            if (isFirefox) {
                baseAnimate = $('html');
            }

            baseAnimate.animate({scrollTop: offset}, 400);

            return false;
        });
    })();
