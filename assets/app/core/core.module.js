/**
 * User: Michael Lazarev <mihailaz.90@gmail.com>
 * Date: 09.08.16
 * Time: 16:30
 */

(function(){
	'use strict';

	angular.module('app.core', [
		'app.templates'
	]);
})();