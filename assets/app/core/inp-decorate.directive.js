/**
 * User: Michael Lazarev <mihailaz.90@gmail.com>
 * Date: 12.08.16
 * Time: 18:44
 */

(function(){
	'use strict';

	angular.module('app.core').directive('inpDecorate', function(){
		return {
			restrict: 'C',
			link    : function($scope, el){
				$(el).styler();
				$scope.$on('$destroy', function() {
					$(el).styler('destroy');
				});
			}
		};
	});
})();