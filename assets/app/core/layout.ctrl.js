/**
 * User: Michael Lazarev <mihailaz.90@gmail.com>
 * Date: 15.08.16
 * Time: 12:47
 */

(function(){
	'use strict';

	angular.module('app.core').controller('LayoutController', function($scope){
		$scope.year    = (new Date()).getFullYear();
		$scope.socials = [{
			name: 'ВКонтакте',
			icon: 'vk',
			link: 'http://vk.com/budrussia'
		}, {
			name: 'Instagram',
			icon: 'in',
			link: 'https://instagram.com/budalcofree/'
		}, {
			name: 'Facebook',
			icon: 'fb',
			link: 'https://www.facebook.com/BudAlcoFree/'
		},{
			name: 'Twitter',
			icon: 'tw',
			link: 'https://twitter.com/budalcofree'
		}];
	});
})();