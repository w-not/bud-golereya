/**
 * User: Michael Lazarev <mihailaz.90@gmail.com>
 * Date: 26.08.16
 * Time: 16:39
 */

(function(){
	'use strict';

	angular.module('app.core').directive('copy', function(){
		return {
			restrict: 'A',
			link    : function($scope, $el, $attrs){
				var clipboard = new Clipboard($el[0], {
					target: function(trigger) {
						return $($attrs.copy)[0];
					}
				});
				clipboard.on('success', function(e){
					var btn  = $(e.trigger),
					    text = btn.attr('data-copied');
					if (text){
						btn.text(text);
					}
					e.clearSelection();
				});
			}
		};
	});
})();