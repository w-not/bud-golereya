/**
 * User: Michael Lazarev <mihailaz.90@gmail.com>
 * Date: 26.08.16
 * Time: 13:54
 */

(function(){
	'use strict';

	angular.module('app.core').directive('mask', function(){
		return {
			restrict: 'A',
			link    : function($scope, $el, $attrs){
				if (!$attrs.mask){
					return;
				}
				$el.mask($attrs.mask);

				$scope.$on('$destroy', function(){
					$($el).unmask();
				});
			}
		};
	});
})();