/**
 * User: Michael Lazarev <mihailaz.90@gmail.com>
 * Date: 12.08.16
 * Time: 17:27
 */


(function(){
	"use strict";

	angular.module('app.core').directive('owlCarousel', function(){
		return {
			restrict  : 'C',
			link      : function($scope, el, $attrs){
				$(el).owlCarousel({
					loop: false,
					margin: 0,
					nav: true,
					items: 1
				});
				$scope.$on('$destroy', function() {
					$(el).data('owlCarousel').destroy();
				});
			}
		};
	});
})();