/**
 * User: Michael Lazarev <mihailaz.90@gmail.com>
 * Date: 23.08.16
 * Time: 19:11
 */

(function(){
	'use strict';

	angular.module('app.core').directive('totab', function(){
		return {
			restrict: 'A',
			link    : function($scope, $el, $attrs){
				var context = $attrs.totabContext ? $($attrs.totabContext) : $(document);

				$($el).on('click', totab);

				$scope.$on('$destroy', function(){
					$($el).off('click', totab);
				});

				function totab(){
					context.find('[data-toggle=tab][data-target="' + $attrs.totab + '"]').click();
				}
			}
		};
	});
})();