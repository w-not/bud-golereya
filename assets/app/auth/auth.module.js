/**
 * User: Michael Lazarev <mihailaz.90@gmail.com>
 * Date: 09.08.16
 * Time: 12:56
 */

(function(){
	'use strict';

	angular.module('app.auth', []);
})();