/**
 * User: Michael Lazarev <mihailaz.90@gmail.com>
 * Date: 12.08.16
 * Time: 14:24
 */

(function(){
	'use strict';

	angular.module('app', [
		'app.core',
		'app.video',
		'app.auth',
		'app.templates'
	]);
})();