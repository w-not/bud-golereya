/**
 * User: Michael Lazarev <mihailaz.90@gmail.com>
 * Date: 12.08.16
 * Time: 15:29
 */

(function(){
	'use strict';

	angular.module('app.video', [
		// 'ngResource',
		'app.core',
		'app.templates'
	]);
})();