/**
 * User: Michael Lazarev <mihailaz.90@gmail.com>
 * Date: 16.08.16
 * Time: 11:28
 */

(function(){
	'use strict';

	angular.module('app.video').directive('jsVideoPopup', function(){

		$(document).on('hide.bs.modal', '#modal_video', function() {
			$(this).find('.modal-iframe').html('');
			$(this).find('[data-video-title]').html('');
			$(this).find('[data-video-desc]').html('');
		});

		function showVideo(){
			var source = $(this).data('src'),
			    html = '<iframe class="iframe-youtube" width="100%" height="100%" src="' +
				    source + '?autoplay=1&controls=2&showinfo=0" frameborder="0" allowfullscreen></iframe>',
				title = $(this).attr('data-video-title') || '',
				desc = $(this).attr('data-video-desc') || '';

			if (!source) {
				return;
			}

			$('#modal_video').modal('show');
			setTimeout(function() {
				var popup = $('#modal_video');

				popup.find('[data-video-title]').html(title);
				popup.find('[data-video-desc]').html(desc);
				popup.find('.modal-iframe')
					.html('<div class="video__iframe">' + html + '</div>');
			}, 500);

			return false;
		}

		return {
			restrict: 'C',
			link    : function($scope, el){
				$(el).on('click', showVideo);
				$scope.$on('$destroy', function() {
					$(el).off('click', showVideo);
				});
			}
		};
	});
})();