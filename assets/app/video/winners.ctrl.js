/**
 * User: Michael Lazarev <mihailaz.90@gmail.com>
 * Date: 12.08.16
 * Time: 15:37
 */

(function(){
	'use strict';

	angular.module('app.video').controller('WinnersController', function($scope){
		$scope.carousel = [{
			name: 'Анубис Нилов',
			date: '20.08.16'
		},{
			name: 'Анубис Нилов 2',
			date: '20.08.16'
		}];

		$scope.owlOptions = {
			loop: false,
			margin: 0,
			nav: true,
			items: 1
		};
	});
})();