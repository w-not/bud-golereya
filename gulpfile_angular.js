/**
 * User: Michael Lazarev <mihailaz.90@gmail.com>
 * Date: 09.08.16
 * Time: 11:39
 */

"use strict";

var gulp        = require('gulp'),
    concat      = require('gulp-concat'),
    uglify      = require('gulp-uglify'),
    htmlmin     = require('gulp-htmlmin'),
    sourcemaps  = require('gulp-sourcemaps'),
    ngAnnotate  = require('gulp-ng-annotate'),
    tmplCache   = require('gulp-angular-templatecache'),
    basePath    = 'assets/',
    publicPath  = 'public',
    destPath    = publicPath + '/app',
    jsPath      = basePath + 'app';

gulp.task('default', ['watch']);
gulp.task('js', ['vendor-js', 'app-js', 'app-tmpls']);
gulp.task('build', ['js']);

gulp.task('watch', ['build'], function(){
	gulp.watch([jsPath + '/**/*.js'], ['app-js']);
	gulp.watch([jsPath + '/**/*.html'], ['app-tmpls']);
});

gulp.task('vendor-js', function(){
	gulp.src([
		basePath + '../bower_components/angular/angular.js',
		// basePath + '../bower_components/angular-route/angular-route.js',
		// basePath + '../bower_components/angular-cookies/angular-cookies.js',
		// basePath + '../bower_components/angular-resource/angular-resource.js',
		// basePath + '../bower_components/angular-ui-router/release/angular-ui-router.js',
		basePath + '../bower_components/clipboard/dist/clipboard.js',
		basePath + '../bower_components/jquery.maskedinput/dist/jquery.maskedinput.js'
	])
		.pipe(sourcemaps.init())
		.pipe(concat('vendor.js'))
		.pipe(uglify())
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest(destPath));
});

gulp.task('app-js', function(){
	gulp.src([
		jsPath + '/**/*.module.js',
		jsPath + '/**/*.js'
	])
		.pipe(sourcemaps.init())
		.pipe(concat('app.js'))
		.pipe(ngAnnotate())
		.pipe(uglify())
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest(destPath));
});

gulp.task('app-tmpls', function () {
	gulp.src(jsPath + '/**/*.html')
		.pipe(htmlmin({collapseWhitespace: true}))
		.pipe(tmplCache({
			filename  : 'templates.js',
			module    : 'app.templates',
			standalone: true
		}))
		.pipe(uglify())
		.pipe(gulp.dest(destPath));
});
